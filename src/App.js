import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
// v5 of react-router-dom: Routes is Switch
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights'
import Home from './pages/Home';
import Courses from './pages/Courses';
import Error from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';

function App() {
  return (
    <Router>
      <AppNavBar/>
      <Container>
      <Routes>
        {/*  <Banner/>
          <Highlights/>*/}
          <Route exact path= "/" element={<Home/>}/>

        {/*v5 routing: <Route exact path= "/" component= {Home}/>*/}

          <Route exact path= "/courses" element={<Courses/>}/>
          <Route exact path= "/register" element={<Register/>}/>
          <Route exact path= "/login" element={<Login/>}/>
          <Route exact path= "/logout" element={<Logout/>}/>
          <Route exact path= "*" element={<Error/>}/>
      </Routes>
      </Container>
    </Router>
  )
}

export default App;
