import {Navigate} from 'react-router-dom'
// v5: Redirect to

export default function Logout(){

	localStorage.clear()
	
	return(
		<Navigate to='/login'/>
	)
}
