
import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Register(){

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Simulation of user registration
	function registerUser(e){
		e.preventDefault();

		//clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!');

	}

	useEffect(() => {

		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);

		} else {
			setIsActive(false);
		}

	}, [email, password1, password2])

	return(

		<Form onSubmit={e => registerUser(e)}>
		
			<h1>Register</h1>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type= "email"
					placeholder= "Enter you email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type= "password"
					placeholder= "Input you password here"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type= "password"
					placeholder= "Input your password again"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
{/* Conditionally */}
			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
				Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
				Submit
				</Button>
			}

		</Form>

		)
}